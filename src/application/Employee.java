package application;

import javafx.beans.property.SimpleStringProperty;

public class Employee {
	public SimpleStringProperty EmployeeId;
	public SimpleStringProperty EmployeeName;
 
    public Employee(String employeeid, String employeename) {
        this.EmployeeId = new SimpleStringProperty(employeeid);
        this.EmployeeName = new SimpleStringProperty(employeename);

    }

	public Employee() {
		this.EmployeeId = new SimpleStringProperty();
        this.EmployeeName = new SimpleStringProperty();
	}

	public String getEmployeeId() {
		return EmployeeId.get();
	}

	public void setEmployeeId(SimpleStringProperty employeeId) {
		EmployeeId = employeeId;
	}

	public String getEmployeeName() {
		return EmployeeName.get();
	}

	public void setEmployeeName(SimpleStringProperty employeeName) {
		EmployeeName = employeeName;
	}
    
    
}
