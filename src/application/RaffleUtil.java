package application;


import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class RaffleUtil {

	 /*
	  * TODO: Add the excel file in the project folder
	  */
	 private  final String FILE_NAME = "Employees.xlsx";

	 
	 public String getEmployeeName(String UID){
		 int i = 0;
		  try {
			FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
			Workbook wb = new XSSFWorkbook(excelFile); 
			  
			 Sheet datatypeSheet = wb.getSheetAt(0);
	         Iterator<Row> iterator = datatypeSheet.iterator();
	         
	         
	         while (iterator.hasNext()) {
	        	 
	        	 //did not include the header
	        	 Row currentRow = datatypeSheet.getRow(++i);
	        	 
	             Iterator<Cell> cellIterator = currentRow.iterator();
	             
	               while (cellIterator.hasNext()) {
	            	   Cell idCell = cellIterator.next();
	            	   Cell employeeNameCell = currentRow.getCell(1);
	            	   
	            	   //check for UID
	            	   if(UID.equalsIgnoreCase(idCell.getStringCellValue())){
	            		   return employeeNameCell.getStringCellValue();
	            	   }
	                }
	         }
	         
		  } catch (Exception e) {
			e.printStackTrace();
		}
		  return null;
	 }
	 
	 
	 public static void openWorkBook() throws Exception { 
	      
	      File file = new File("Employees.xlsx");
	      Workbook wb = new XSSFWorkbook();
	      CreationHelper createHelper = wb.getCreationHelper();
	      
	      if(!(file.isFile() && file.exists())) {
	    	 Sheet sheet = wb.createSheet("new sheet");

		  // Create a row and put some cells in it. Rows are 0 based.
		 	 Row row = sheet.createRow((short) 0);
		 	 row.createCell(0).setCellValue(createHelper.createRichTextString("ID"));
		 	 row.createCell(1).setCellValue(createHelper.createRichTextString("Name"));
//		 	 row.createCell(2).setCellValue(createHelper.createRichTextString("Prizes"));

		  // Write the output to a file
			 FileOutputStream fOut = new FileOutputStream("Employees.xlsx");
		 	 wb.write(fOut);
		 	 fOut.close();
		 	 wb.close();
	      } 
	      
	      try {
	            Desktop.getDesktop().open(new File("Employees.xlsx"));
	            
	        } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	      
	   }

      
}
