package application.view;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import application.SQLiteJDBCDriverConnection;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.util.Duration;


public class NormalRaffleController {

	@FXML
	private Label firstchar;
	@FXML
	private Label secondchar;
	@FXML
	private Label thirdchar;
	@FXML
	private Label fourthchar;
	@FXML
	private Label fifthchar;
	@FXML
	private Label sixthchar;
	@FXML
	private Label Congratulation;
	@FXML
	private Button button;
	@FXML
	private Button Stopbtn;
	@FXML
	private Button StartAgainbtn;
	@FXML
	private Label EmployeeNameText;
	@FXML
	private Label CountDownTenText;
	
	
	private static String winner;
	private static String employeeName;
	private static String RaffleStatus = "Done";


	private static Timeline labelstylewinner;
	private static Timeline firstlabel;
	private static Timeline secondlabel;
	private static Timeline thirdlabel;
	private static Timeline fourthlabel;
	private static Timeline fifthlabel;
	private static Timeline sixthlabel;
	private static Timeline setsecondChar;
	private static Timeline setthirdChar;
	private static Timeline setfourthChar;
	private static Timeline setfifthChar;
	private static Timeline setsixthChar;
	private static Timeline employeenameTL;
	private static Timeline countdowntenTL;
	
	@FXML
	public void showWinner()
	{
		 
		
		clearAll();
		initializeLabels();
		ShowBox();
	    bindToLabels();
	    moveWinner();
	    deleteOldWinnerId();
		
	   
	}
	
	@FXML
	public void StopCountDown(){
		clearAll();
		countdowntenTL.stop();
		HideBox();
		RaffleStatus="Done";
		
	}
	
	public String GetStaus(){
		return RaffleStatus;
	}
	

	private void HideBox(){
		firstchar.setVisible(false);
		secondchar.setVisible(false);
		thirdchar.setVisible(false);
		fourthchar.setVisible(false);
		fifthchar.setVisible(false);
		sixthchar.setVisible(false);
		Congratulation.setVisible(true);
		EmployeeNameSetup(true);
	
	}
	
	private void ShowBox(){
		firstchar.setVisible(true);
		secondchar.setVisible(true);
		thirdchar.setVisible(true);
		fourthchar.setVisible(true);
		fifthchar.setVisible(true);
		sixthchar.setVisible(true);
		Congratulation.setVisible(false);
		EmployeeNameSetup(false);
		
	}
	
	private void EmployeeNameSetup(boolean Boolean){
		if(Boolean){
			EmployeeNameText.setLayoutY(300);
			EmployeeNameText.setText(employeeName);
			EmployeeNameText.setVisible(true);
		}
		else{
			EmployeeNameText.setLayoutY(315);	
			EmployeeNameText.setVisible(false);
			EmployeeNameText.setText("");
		}
	}
	
	
	
	
	private void clearAll() {

		if(labelstylewinner != null) labelstylewinner.stop();
		if(firstlabel != null) firstlabel.stop();
		if(secondlabel != null) secondlabel.stop();
		if(thirdlabel != null) thirdlabel.stop();
		if(fourthlabel != null) fourthlabel.stop();
		if(fifthlabel != null) fifthlabel.stop();
		if(sixthlabel != null) sixthlabel.stop();
		if(setsecondChar != null) setsecondChar.stop();
		if(setthirdChar != null) setthirdChar.stop();
		if(setfourthChar != null) setfourthChar.stop();
		if(setfifthChar != null) setfifthChar.stop();
		if(setsixthChar != null) setsixthChar.stop();
		}
	
	private void bindToLabels() {
		winner = randomEmployee().toUpperCase();
		RaffleStatus="Raffle";
		
//first Character
		EventHandler<ActionEvent> eventHandler1 = e -> firstchar.setText(winner.substring(0,1));
	//	EventHandler<ActionEvent> eventHandlerstyle1 = e -> firstchar.setStyle("-fx-border-color : blue; -fx-border-radius : 5px; -fx-border-width : 4px ");
		EventHandler<ActionEvent> eventHandlerstylebutton = e -> button.setDisable(true);
		firstlabel = new Timeline(new KeyFrame(Duration.millis(100), eventHandler1), 
				new KeyFrame(Duration.millis(100), eventHandlerstylebutton));
	//			new KeyFrame(Duration.millis(100), eventHandlerstyle1));
		firstlabel.play();
		
//Second Character
	    secondlabel = new Timeline(
	      new KeyFrame(Duration.seconds(0),
	        new EventHandler<ActionEvent>() {
	          @Override 
	          public void handle(ActionEvent actionEvent) {
	        	  String[] n = {"0","1","2","3","4","5","6","7","8","9"};
	        	  Random rand = new Random();
	        	  int x = rand.nextInt(n.length);
	        	  secondchar.setText(n[x]);
	          }
	        }
	      ),
	      new KeyFrame(Duration.millis(100))
	      
	    );
	    secondlabel.setCycleCount(30);
	    secondlabel.play();
	    
		EventHandler<ActionEvent> eventHandler2 = e -> secondchar.setText(winner.substring(1,2));
	//	EventHandler<ActionEvent> eventHandlerstyle2 = e -> secondchar.setStyle("-fx-border-color : blue; -fx-border-radius : 5px; -fx-border-width : 4px ");
	    setsecondChar = new Timeline(
	    		new KeyFrame(Duration.millis(3200), eventHandler2));
	//    		new KeyFrame(Duration.millis(3300), eventHandlerstyle2));
	    setsecondChar.play();
	    
//Third Character
	    thirdlabel = new Timeline(
	      new KeyFrame(Duration.seconds(0),
	        new EventHandler<ActionEvent>() {
	          @Override 
	          public void handle(ActionEvent actionEvent) {
	        	  String[] n = {"0","1","2","3","4","5","6","7","8","9"};
	        	  Random rand = new Random();
	        	  int x = rand.nextInt(n.length);
	        	  thirdchar.setText(n[x]);
	          }
	        }
	      ),
	      new KeyFrame(Duration.millis(100))
	      
	    );
	    thirdlabel.setCycleCount(50);
	    thirdlabel.play();
	    
		EventHandler<ActionEvent> eventHandler3 = e -> thirdchar.setText(winner.substring(2,3));
	//	EventHandler<ActionEvent> eventHandlerstyle3 = e -> thirdchar.setStyle("-fx-border-color : blue; -fx-border-radius : 5px; -fx-border-width : 4px ");
	    setthirdChar = new Timeline(
	    		new KeyFrame(Duration.millis(5200), eventHandler3));
	//			new KeyFrame(Duration.millis(5200), eventHandlerstyle3));
	    setthirdChar.play();
	    
//fourth Character
	    fourthlabel = new Timeline(
	      new KeyFrame(Duration.seconds(0),
	        new EventHandler<ActionEvent>() {
	          @Override 
	          public void handle(ActionEvent actionEvent) {
	        	  String[] n = {"0","1","2","3","4","5","6","7","8","9"};
	        	  Random rand = new Random();
	        	  int x = rand.nextInt(n.length);
	        	  fourthchar.setText(n[x]);
	          }
	        }
	      ),
	      new KeyFrame(Duration.millis(100))
	      
	    );
	    fourthlabel.setCycleCount(70);
	    fourthlabel.play();
	    
		EventHandler<ActionEvent> eventHandler4 = e -> fourthchar.setText(winner.substring(3,4));
	//	EventHandler<ActionEvent> eventHandlerstyle4 = e -> fourthchar.setStyle("-fx-border-color : blue; -fx-border-radius : 5px; -fx-border-width : 4px ");
	    setfourthChar = new Timeline(
	    		new KeyFrame(Duration.millis(7200), eventHandler4));
	//    		new KeyFrame(Duration.millis(7200), eventHandlerstyle4));
	    setfourthChar.play();
	    
//fifth Character
	    fifthlabel = new Timeline(
	      new KeyFrame(Duration.seconds(0),
	        new EventHandler<ActionEvent>() {
	          @Override 
	          public void handle(ActionEvent actionEvent) {
	        	  String[] n = {"0","1","2","3","4","5","6","7","8","9"};
	        	  Random rand = new Random();
	        	  int x = rand.nextInt(n.length);
	        	  fifthchar.setText(n[x]);
	          }
	        }
	      ),
	      new KeyFrame(Duration.millis(100))
	      
	    );
	    fifthlabel.setCycleCount(90);
	    fifthlabel.play();
	    
		EventHandler<ActionEvent> eventHandler5 = e -> fifthchar.setText(winner.substring(4,5));
	//	EventHandler<ActionEvent> eventHandlerstyle5 = e -> fifthchar.setStyle("-fx-border-color : blue; -fx-border-radius : 5px; -fx-border-width : 4px ");
	    setfifthChar = new Timeline(
	    		new KeyFrame(Duration.millis(9200), eventHandler5));
	//    		new KeyFrame(Duration.millis(9200), eventHandlerstyle5));
	    setfifthChar.play();
	    
//sixth Character
	    sixthlabel = new Timeline(
	      new KeyFrame(Duration.seconds(0),
	        new EventHandler<ActionEvent>() {
	          @Override public void handle(ActionEvent actionEvent) {
	        	  String[] n = {"0","1","2","3","4","5","6","7","8","9"};
	        	  Random rand = new Random();
	        	  int x = rand.nextInt(n.length);
	        	  sixthchar.setText(n[x]);
	          }
	        }
	      ),
	      new KeyFrame(Duration.millis(100))
	      
	    );
	    sixthlabel.setCycleCount(110);
	    sixthlabel.play();
	    
		EventHandler<ActionEvent> eventHandler6 = e -> sixthchar.setText(winner.substring(5,6));
	//	EventHandler<ActionEvent> eventHandlerstyle6 = e -> sixthchar.setStyle("-fx-border-color : blue; -fx-border-radius : 5px; -fx-border-width : 4px ");
	//	EventHandler<ActionEvent> eventHandlerstylebutton6 = e -> button.setDisable(false);
	    setsixthChar = new Timeline(
	    		new KeyFrame(Duration.millis(11200), eventHandler6));
	//    		new KeyFrame(Duration.millis(11200), eventHandlerstylebutton6),
	//			new KeyFrame(Duration.millis(11200), eventHandlerstyle6));
	    setsixthChar.play();
	    

		//Employee Name
		employeenameTL = new Timeline(new KeyFrame(Duration.millis(11300), new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				EmployeeNameText.setText(employeeName);
				EmployeeNameText.setVisible(true);
				CountDownTenText.setVisible(true);
				countdowntenTL.setCycleCount(11);
				countdowntenTL.play();
			}
			
		}), new KeyFrame(Duration.millis(100)));
		
		employeenameTL.play();
		
		//Count 10
		countdowntenTL = new Timeline(new KeyFrame(Duration.millis(1200), new EventHandler<ActionEvent>() {
			int x =0;
			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				RaffleStatus="CountDown";
				String[] n = { "10", "9", "8", "7", "6", "5", "4", "3", "2", "1","0" };
				CountDownTenText.setText(n[x++]);
				
				
				if(CountDownTenText.getText().equals("0")){
					RaffleStatus="Done";
				}
				
			}
			
		}), new KeyFrame(Duration.millis(100)));
									
	}
	    
//style label
//	    labelstylewinner = new Timeline(
//		  	      new KeyFrame(Duration.seconds(0),
//		  	        new EventHandler<ActionEvent>() {
//		  	          @Override 
//		  	          public void handle(ActionEvent actionEvent) {
//		  	        	  String[] n = {"-fx-border-color : blue;-fx-border-radius : 5px; -fx-border-width : 4px;",
//		  	        			  "-fx-border-color : red; -fx-border-radius : 5px; -fx-border-width : 4px;"};
//		  	        	  Random rand = new Random();
//		  	        	  int x = rand.nextInt(n.length);
//		  	        	  String style;
//		  	        	  if(n[x].equals("-fx-border-color : blue;-fx-border-radius : 5px; -fx-border-width : 4px;")) {
//		  	        		  style = "-fx-border-color : red;-fx-border-radius : 5px; -fx-border-width : 4px;";
//		  	        	  }else {
//		  	        		  style = "-fx-border-color : blue;-fx-border-radius : 5px; -fx-border-width : 4px;";
//		  	      	      }
//		  	        	  firstchar.setStyle(style);
//		  	        	  secondchar.setStyle(style);
//		  	        	  thirdchar.setStyle(style);
//		  	        	  fourthchar.setStyle(style);
//		  	        	  fifthchar.setStyle(style);
//		  	        	  sixthchar.setStyle(style);
//
//		  	          }
//		  	        }
//		  	      ),
//		  	      new KeyFrame(Duration.millis(100))
//		  	      
//		  	    );
//		    	labelstylewinner.setCycleCount(3000);
//		    	labelstylewinner.play();

	
	
	
//	@FXML
//	public void showWinnerManual(){
//	
//		winner = randomEmployee();
//		
//		if(firstchar.getText().equals("")) {
//			firstchar.setText(winner.substring(0,1));
//		}
//		else if(secondchar.getText().equals("")) {
//			secondchar.setText(winner.substring(1,2));
//		}
//		else if(thirdchar.getText().equals("")) {
//			thirdchar.setText(winner.substring(2,3));
//		}
//		else if(fourthchar.getText().equals("")) {
//			fourthchar.setText(winner.substring(3,4));
//		}
//		else if(fifthchar.getText().equals("")) {
//			fifthchar.setText(winner.substring(4,5));
//		}
//		else if(sixthchar.getText().equals("")) {
//			sixthchar.setText(winner.substring(5,6));
//		}
//		else {
//			firstchar.setText("");
//		    secondchar.setText("");
//		    thirdchar.setText("");
//		    fourthchar.setText("");
//		    fifthchar.setText("");
//		    sixthchar.setText(""); 
//		}
//		
//		
//	    //Move winner id to winners table
//	    moveWinner();
//	    deleteOldWinnerId();
//
//	}
	
	private void initializeLabels() {
		//Clear field
	    firstchar.setText("");
	    secondchar.setText("");
	    thirdchar.setText("");
	    fourthchar.setText("");
	    fifthchar.setText("");
	    sixthchar.setText(""); 
//	    
//	    firstchar.setStyle("-fx-border-color : black; -fx-border-radius : 5px; -fx-border-width : 1px ");
//	    secondchar.setStyle("-fx-border-color : black; -fx-border-radius : 5px; -fx-border-width : 1px ");
//	    thirdchar.setStyle("-fx-border-color : black; -fx-border-radius : 5px; -fx-border-width : 1px ");
//	    fourthchar.setStyle("-fx-border-color : black; -fx-border-radius : 5px; -fx-border-width : 1px ");
//	    fifthchar.setStyle("-fx-border-color : black; -fx-border-radius : 5px; -fx-border-width : 1px ");
//	    sixthchar.setStyle("-fx-border-color : black; -fx-border-radius : 5px; -fx-border-width : 1px ");
	}
	
	public static String randomEmployee() {
		
		Random rand = new Random();
		int tablesize = dataCount();
		int n = rand.nextInt(tablesize);
    	String employeewinner = (String) SQLiteJDBCDriverConnection.getEmployees().get(n);
    	employeeName = (String) SQLiteJDBCDriverConnection.getEmployeesName().get(n);
		return employeewinner;
    }
	
	public static int dataCount() {
		
		int count = 0;
        // SQLite connection string
        String url = "jdbc:sqlite:list.db";
        
        // SQL statement for creating a new table
        String sql = "SELECT COUNT(*) as totcount FROM employees";
        
        try (Connection conn = DriverManager.getConnection(url);
        	Statement stmt = conn.createStatement()) {
        	ResultSet rs = conn.createStatement().executeQuery(sql);
        	count = rs.getInt("totcount");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
		return count;
    }
	
	public static void moveWinner() {
        // SQLite connection string
        String url = "jdbc:sqlite:list.db";
        
        // SQL statement for creating a new table
        String sql = "INSERT INTO winners SELECT * FROM employees WHERE employeeid = " + "'" + winner + "'";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Winner successfully moved.");
    }
	
	public static void deleteOldWinnerId() {
        // SQLite connection string
        String url = "jdbc:sqlite:list.db";
        
        // SQL statement for creating a new table
        String sql = "DELETE FROM employees WHERE employeeid = " + "'" + winner + "'";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Winner successfully deleted from employees table.");
    }
	
}


