package application.view;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import application.Main;
import application.RaffleUtil;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.layout.Region;

public class MainViewController {
	
	String User;
	
	@FXML
	private Button registerButton;
	
	@FXML
	private Button employeesButton;

	@FXML
	private Button excelButton;
	
	@FXML
	private Button raffleButton;
	
	@FXML
	private Button raffleButtonUser;
	
	@FXML
	private Button winnersButton;
	
	@FXML
	private PasswordField adminpass;
	
	
	@FXML
	public void goRegister() throws IOException {
		Main.showRegister();
	}
	
	@FXML
	private void goNormalRaffle() throws IOException {
		Main.showNormalRaffle();
	}
	
	@FXML
	private void goNormalRaffleUser() throws IOException {
		Main.showNormalRaffleUser();
	}
	
	@FXML
	private void goEmployeeList() throws IOException {
		Main.showEmployeeList();
	}
	
	@FXML
	private void goWinnerList() throws IOException {
		Main.showWinnerList();
	}
	
	@FXML
	private void goExcel() throws Exception {
		RaffleUtil.openWorkBook();
	}
	
	
	
	@FXML
	private void Login() throws IOException{
		String password = adminpass.getText();
		if(password.equals("adminP@55w0rd")){
			registerButton.setDisable(false);
			employeesButton.setDisable(false);
			excelButton.setDisable(false);
			raffleButton.setDisable(false);
			raffleButton.setVisible(true);
			winnersButton.setDisable(false);
			User="ItsMyLife";
			goRegister();
		}
		else if(password.equals("user")){
			registerButton.setDisable(false);
			raffleButtonUser.setDisable(false);
			raffleButtonUser.setVisible(true);
			User="user1234";
			goRegister();
		}
		else{
			Alert alert = new Alert(AlertType.NONE, "Password incorrect.", ButtonType.OK);
			alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
			alert.show();
		}
	}
	

}
