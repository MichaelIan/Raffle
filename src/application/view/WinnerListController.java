package application.view;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import application.Employee;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;


public class WinnerListController implements Initializable {
	@FXML 
	private TableView<Employee> tableView;
	@FXML 
	private TableColumn<Employee,String> winnerIdCol;
	@FXML 
	private TableColumn<Employee,String> winnerNameCol;
	public static ObservableList<Employee> winnerdata;
	
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	
    	Connection conn = null;
    	winnerdata = FXCollections.observableArrayList(); 
    	
        //set up the columns in the table
    	winnerIdCol.setCellValueFactory(new PropertyValueFactory<Employee, String>("EmployeeId"));
    	winnerNameCol.setCellValueFactory(new PropertyValueFactory<Employee, String>("EmployeeName"));
    	
    	try {
            // create a connection to the database
            conn = DriverManager.getConnection("jdbc:sqlite:list.db");
            System.out.println("Connection to SQLite has been established. <WinnerListController>");
            String sql = "SELECT employeeid, employeename FROM winners";
            ResultSet rs = conn.createStatement().executeQuery(sql);
            while(rs.next()) {
            	Employee emp = new Employee();
            	emp.EmployeeId.set(rs.getString("employeeid"));
            	emp.EmployeeName.set(rs.getString("employeename"));
            	winnerdata.add(emp);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

    	//Load data
    	tableView.setItems(winnerdata);
    	
    }
    
    @FXML
    private void deleteEmployee(){
    	int selectedEmployeeIndex = tableView.getSelectionModel().getSelectedIndex();
    	Employee selectedEmployee = tableView.getSelectionModel().getSelectedItem();
    
    	if(selectedEmployeeIndex >= 0){
    		String selectedWinner = selectedEmployee.getEmployeeId().toString();
    		tableView.getItems().remove(selectedEmployeeIndex);
    		deleteSelectedWinnerId(selectedWinner);
    		
    	}
    }
    
    public static void deleteSelectedWinnerId(String removeSelected) {
        // SQLite connection string
        String url = "jdbc:sqlite:list.db";
        
        // SQL statement for creating a new table
        String sql = "DELETE FROM winners WHERE employeeid = " + "'" + removeSelected + "'";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Successfully deleted selected row");
    }

}


