package application.view;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import application.RaffleUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;

public class RegisterController {
	
	@FXML
	private TextField uidtext;
	@FXML
	private TextField nametext;
	@FXML
	private ImageView imageview;
	
	public class InsertApp {
	    
        // Connect to the list.db database  
        private Connection connect() {
            // SQLite connection string
            String url = "jdbc:sqlite:list.db";
            Connection conn = null;
            try {
                conn = DriverManager.getConnection(url);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            return conn;
        }
   
        // Insert a new row into the employees table
        public void insert(String employeeid, String employeename) {
            String sql = "INSERT INTO employees(employeeid,employeename) VALUES(?,?)";
            
     
            try (Connection conn = this.connect();
                    PreparedStatement pstmt = conn.prepareStatement(sql)) {
                pstmt.setString(1, employeeid);
                pstmt.setString(2, employeename);
                pstmt.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }
		
	public void register() {
		InsertApp  register= new InsertApp();
		
		int unique = 0;
		Connection conn=null;
		
		try {
            // create a connection to the database
            conn = DriverManager.getConnection("jdbc:sqlite:list.db");
            System.out.println("Connection to SQLite has been established. <EmployeeListController>");
            String sql = "SELECT employeeid FROM employees";
            ResultSet rs = conn.createStatement().executeQuery(sql);
            while(rs.next()) {
            	String employeesUid = rs.getString("employeeid");
            	if(uidtext.getText().equalsIgnoreCase(employeesUid)) {
    				unique = unique + 1;
    			}
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
		
		if(unique == 0 && uidtext.getText().length() == 6 && nametext.getText().length() != 0) {
			register.insert(uidtext.getText().toUpperCase(), nametext.getText());
			uidtext.clear();
			nametext.clear();
				
			Alert alert = new Alert(AlertType.NONE, "Successfully registered.", ButtonType.OK);
			alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
			alert.show();
			
		}
		else if(uidtext.getText().length() < 6 || nametext.getText() == null){
			Alert alert = new Alert(AlertType.NONE, "Please enter a valid ID.", ButtonType.OK);
			alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
			alert.show();
		}
		else {
			Alert alert = new Alert(AlertType.NONE, "Duplicate Entry.", ButtonType.OK);
			alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
			alert.show();
		}			
	}
	
	public void textCharLimit() {
			if(uidtext.getText().length() > 6) {
				uidtext.setText(uidtext.getText().substring(0,6));
				uidtext.positionCaret(uidtext.getLength());
			}
			else if(uidtext.getText().length() == 6){
				displayName();
			}

	
	}
	
	public void displayName() {
		RaffleUtil raf = new RaffleUtil();
		String empid = uidtext.getText();
		String empname = raf.getEmployeeName(empid);
		
			if(empid.length() == 6 && empname != null) {
				nametext.setText(empname);
			}
			else{
				nametext.setText("");
				Alert alert = new Alert(AlertType.NONE, "Does not exist.", ButtonType.OK);
				alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
				alert.show();
				
			}
	
		
	}
		
}
