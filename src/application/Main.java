package application;
	
import java.io.IOException;
import java.util.List;

import application.view.MainViewController;
import application.view.NormalRaffleController;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;


public class Main extends Application implements EventHandler<KeyEvent> {
	

	private static Stage primaryStage;
	private static BorderPane mainLayout;
	public Button normalraffle;
	private static String Window;
	private static String Users;
	
	
	@Override
	public void start(Stage primaryStage) throws IOException {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Raffle System");
//		Main.primaryStage.setResizable(false);
		showMainView();
		
	}
	
	private void showMainView() throws IOException {
		Window = "main";
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource(getLoader()));
		mainLayout = loader.load();
		Scene scene = new Scene(mainLayout);
		scene.setOnKeyPressed(this);
		scene.getStylesheets().add(Main.class.getResource("bootstrap3.css").toExternalForm());
		primaryStage.setScene(scene);		
		primaryStage.show();
		
		
	}
	

	
	public static void showRegister() throws IOException {
		Window = "reg";
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/Register.fxml"));
		AnchorPane register = loader.load();
		mainLayout.setCenter(register);
		
		
	}
	
	public static void showNormalRaffle() throws IOException {
		Window = "raffle";
		Users= "admin";
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/NormalRaffle.fxml"));
		AnchorPane normalraffle = loader.load();
		mainLayout.setCenter(normalraffle);	
		
	}
	
	public static void showNormalRaffleUser() throws IOException {
		Window = "raffle";
		Users= "user";
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/NormalRaffle.fxml"));
		AnchorPane normalraffle = loader.load();
		mainLayout.setCenter(normalraffle);	
	}
	
	public static void showEmployeeList() throws IOException {
		Window = "employee";
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/EmployeeList.fxml"));
		AnchorPane employeelist = loader.load();
		mainLayout.setCenter(employeelist);		
	}
	
	public static void showWinnerList() throws IOException {
		Window = "winner";
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/WinnerList.fxml"));
		AnchorPane winnerlist = loader.load();
		mainLayout.setCenter(winnerlist);		
	}
	
	public static void HideToolBar() throws IOException {
		Window = "HideToolBar";		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/Blank.fxml"));
		AnchorPane winnerlist = loader.load();
		mainLayout.setTop(winnerlist);		
	}
	
	
	public static void main(String[] args) {
		
		SQLiteJDBCDriverConnection.connect();
		SQLiteJDBCDriverConnection.createEmployeeListTable();
		SQLiteJDBCDriverConnection.createWinnerListTable();
		launch(args);
		
	}
	
	
	 public static String getLoader(){
		 String list = null;
		 
		 if(Users==null){
			 list="view/MainView.fxml";
			 
		 }
		 else if(Users.equals("admin")){
			 list="view/AdminMain.fxml";
		 }
		 else if(Users.equals("user")){
			 list="view/UserMain.fxml";
		 }
		 
		System.out.println(list); 
		return list; 
	 }

	@Override
	public void handle(KeyEvent event){
		// TODO Auto-generated method stub
		System.out.println(event.getCode());
		
		FXMLLoader loader = new FXMLLoader();
		
		AnchorPane normalraffle;
		
			if(Window.equals("raffle") || Window.equals("HideToolBar") ){
				switch(event.getCode()){
				case F1:
					loader.setLocation(Main.class.getResource("view/NormalRaffle.fxml"));
					try {	
						normalraffle = loader.load();	
						NormalRaffleController controller = loader.getController();
						if(controller.GetStaus().equals("Done")){
						if(Window.equals("raffle")){
							HideToolBar();
						}
						else{
							showMainView();
							if(Users.equals("admin")){
								showNormalRaffle();
							}
							else{
								showNormalRaffleUser();
							}
						}
						}
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				
					break;
				case F2:	
						loader.setLocation(Main.class.getResource("view/NormalRaffle.fxml"));
					try {
						normalraffle = loader.load();	
						NormalRaffleController controller = loader.getController();	
						if(controller.GetStaus().equals("Done")){
							mainLayout.setCenter(normalraffle);
							controller.showWinner();	
						}
																		
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				case F3:	
						loader.setLocation(Main.class.getResource("view/NormalRaffle.fxml"));
					try {
						normalraffle = loader.load();	
						NormalRaffleController controller = loader.getController();
						if(controller.GetStaus().equals("CountDown")){
							mainLayout.setCenter(normalraffle);
							controller.StopCountDown();
						}													
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				case F4:
					if(!primaryStage.isFullScreen()){
						primaryStage.setFullScreen(true);
					}
					else{
						primaryStage.setFullScreen(false);
					}
				break;	
				
				default:
					break;
				}
			}
			
		
	}
}
