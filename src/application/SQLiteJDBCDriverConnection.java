package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
 
public class SQLiteJDBCDriverConnection {
    
	//connect to the database
    public static void connect() {
        Connection conn = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:list.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);
            
            System.out.println("Connection to SQLite has been established. <SQLiteJDBCDriverConnection>");
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
    
    public static void createEmployeeListTable() {
        // SQLite connection string
        String url = "jdbc:sqlite:list.db";
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS employees (\n"
                + "	employeeid text NOT NULL,\n"
                + "	employeename text NOT NULL\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Table employees has been created.");
    }
    
    public static void createWinnerListTable() {
        // SQLite connection string
        String url = "jdbc:sqlite:list.db";
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS winners (\n"
                + "	employeeid text NOT NULL,\n"
                + "	employeename text NOT NULL\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Table winners has been created.");
    }
    
    public static List<String> getEmployees(){
    	
    	Connection conn = null;
    	List<String> list = new ArrayList<>();
		try {
            // create a connection to the database
            conn  = DriverManager.getConnection("jdbc:sqlite:list.db");
            System.out.println("Connection to SQLite has been established. <EmployeeListController>");
            String sql = "SELECT employeeid, employeename FROM employees";
            ResultSet rs = conn.createStatement().executeQuery(sql);
            
            while(rs.next()){
            	list.add(rs.getString("employeeid"));
            }	

            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            	}
        }
		return list;
    	
    }
    
 public static List<String> getEmployeesName(){
    	
    	Connection conn = null;
    	List<String> list = new ArrayList<>();
		try {
            // create a connection to the database
            conn  = DriverManager.getConnection("jdbc:sqlite:list.db");
            System.out.println("Connection to SQLite has been established. <EmployeeListController>");
            String sql = "SELECT employeeid, employeename FROM employees";
            ResultSet rs = conn.createStatement().executeQuery(sql);
            
            while(rs.next()){
            	list.add(rs.getString("employeename"));
            }	

            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            	}
        }
		return list;
    	
    }
    
}